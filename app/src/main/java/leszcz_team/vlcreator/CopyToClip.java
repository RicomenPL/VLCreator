package leszcz_team.vlcreator;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.AsyncTask;
import android.widget.Toast;

import static android.content.Context.CLIPBOARD_SERVICE;


public class CopyToClip extends AsyncTask <Void, Void, Void>{

    Activity activity;
    String copyText;
    String toastText;
    String label;

    CopyToClip (Activity mainAc, String text, String labelText){

        this.activity = mainAc;
        copyText = text;
        label = labelText;
        toastText = activity.getString(R.string.copied);

    }

    @Override
    protected Void doInBackground(Void... voids) {

        ClipboardManager clip = (ClipboardManager) activity.getSystemService(CLIPBOARD_SERVICE);
        ClipData dataClip = ClipData.newPlainText(label, copyText);
        clip.setPrimaryClip(dataClip);

        return null;
    }

    @Override
    protected void onPostExecute(Void result){
        Toast.makeText(activity,toastText,Toast.LENGTH_LONG ).show();
    }
}
