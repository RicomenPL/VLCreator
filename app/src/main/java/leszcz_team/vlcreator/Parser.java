package leszcz_team.vlcreator;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;


class Parser extends AsyncTask<Void, Void, Void> {

    boolean check = false;
    String link;
    String end;
    boolean correct;

    Activity activiti;

    Parser(Activity mainAc, String adressURL) {

        this.activiti = mainAc;
        link = adressURL;
    }


    protected void onPreExecute(){

        Spelling spelling = new Spelling(link, activiti);
        correct =  spelling.check();
        link = spelling.getLink();

    }

    @Override
    protected Void doInBackground(Void... params) {

        if (correct) {
            try {
                check = false;
                Document doc = Jsoup.connect(link).get();
                Extracting extracting = new Extracting(doc);
                end = extracting.start();
            } catch (IOException e) {
                check = true;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (check) {
            String checkText = activiti.getString(R.string.connections);
            Toast.makeText(activiti,checkText, Toast.LENGTH_LONG).show();
        }

        EditText resultsText = (EditText) activiti.findViewById(R.id.resultsText);
        resultsText.setText(end);
    }

}
