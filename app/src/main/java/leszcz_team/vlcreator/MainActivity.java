package leszcz_team.vlcreator;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener;
import com.karumi.dexter.listener.single.PermissionListener;

public class MainActivity extends AppCompatActivity {

    public TextView textView;
    public TextView results;
    public EditText editText;
    public EditText resultsText;
    public Button agreeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);
        results = (TextView) findViewById(R.id.results);
        editText = (EditText) findViewById(R.id.editText);
        resultsText = (EditText) findViewById(R.id.resultsText);
        agreeButton = (Button) findViewById(R.id.agreeButton);
    }

    public void agree (View a) {

        String URLString = editText.getText().toString();
        new Parser(this,URLString).execute();

    }

    public void copyToClip (View b) {

        String text = resultsText.getText().toString();
        Spelling spell2 = new Spelling("",this);

        if (spell2.checkResults(text)){
            new CopyToClip(this,text,"URL").execute();
        }

    }

    public void downloadVideo (View c) {

        PermissionListener dialogPermissionListener =
                DialogOnDeniedPermissionListener.Builder
                        .withContext(this)
                        .withTitle(R.string.titleStory)
                        .withMessage(R.string.storyPermission)
                        .withButtonText("OK")
                        .build();

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(dialogPermissionListener)
                .onSameThread()
                .check();

        String URLString2 = editText.getText().toString();
        String text2 = resultsText.getText().toString();

        Spelling spell = new Spelling(URLString2,this);

        if (spell.check() && spell.checkResults(text2)) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                URLString2 = spell.getLink();
                new Download(text2, URLString2, this).execute();
            }
        }
    }
}
