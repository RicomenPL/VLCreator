package leszcz_team.vlcreator;

import android.app.Activity;
import android.widget.Toast;


public class Spelling {

    String link;
    Activity activiti;

    Spelling (String url, Activity mainAC){

        link = url;
        this.activiti = mainAC;
    }

    public boolean check (){

        String emptyURL = activiti.getString(R.string.emptyURL);
        String invalidURL = activiti.getString(R.string.invalidURL);

        String checkString2 = "";
        String checkString3 = "";

        if (link.equals("") || link.length() < 7 ){

            if (link.length() < 7 && link.length() > 0){

                Toast.makeText(activiti,invalidURL, Toast.LENGTH_LONG).show();
                return false;
            }

            Toast.makeText(activiti,emptyURL, Toast.LENGTH_LONG).show();
            return false;
        }

        String checkString = link.substring(0,7);

        if (!checkString.equals("http://")){
            link = "http://" + link;
        }


        if(link.length() >  24) {

            checkString2 = link.substring(0, 24);
            checkString3 = link.substring(0, 22);

        }

        if (checkString2.equals("http://www.cda.pl/video/") || checkString3.equals("http://m.cda.pl/video/")){
            return true;
        }

        Toast.makeText(activiti,invalidURL, Toast.LENGTH_LONG).show();
        return false;

    }

    public boolean checkResults(String results){

        String emptyURL = activiti.getString(R.string.emptyURL);
        String invalidURL = activiti.getString(R.string.invalidURL);

        if (results.equals("") || results.length() < 7 ){

            if (results.length() < 7 && results.length() > 0){

                Toast.makeText(activiti,invalidURL, Toast.LENGTH_LONG).show();
                return false;
            }

            Toast.makeText(activiti,emptyURL, Toast.LENGTH_LONG).show();
            return false;
        }

        String checkString = results.substring(0,7);

        if (!checkString.equals("http://")){
            Toast.makeText(activiti,invalidURL, Toast.LENGTH_LONG).show();
            return false;
        }

        return true;

    }

    public String getLink(){
        return link;
    }
}
