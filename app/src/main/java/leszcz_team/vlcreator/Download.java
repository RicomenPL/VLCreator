package leszcz_team.vlcreator;


import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class Download extends AsyncTask<Void, Void, Void> {

    String url;
    String urlTitle;
    boolean check = false;
    String title;
    Activity activity;


    Download (String URLtext, String URLtitle, Activity mainAC){

        this.activity = mainAC;
        url = URLtext;
        urlTitle = URLtitle;
    }


    @Override
    protected Void doInBackground(Void... voids) {

        extractTitle();
        Start();

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (check) {
            String checkText = activity.getString(R.string.connections);
            Toast.makeText(activity, checkText, Toast.LENGTH_LONG).show();
        }
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(activity,"Brak uprawnień do zapisywania plików w pamieci",Toast.LENGTH_LONG).show();
        }

        String beginDownload = activity.getString(R.string.downloadTitle);
        Toast.makeText(activity,beginDownload,Toast.LENGTH_LONG).show();
    }

    public void Start(){

        String description = activity.getString(R.string.descriptionDownload);

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription(description);
        request.setTitle(title);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, title + ".mp4");

        DownloadManager manager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);

    }

    public void extractTitle(){
        try {
            Document doc = Jsoup.connect(urlTitle).get();
            title = doc.title();

        }
        catch (IOException e){
            check = true;
        }
    }
}
