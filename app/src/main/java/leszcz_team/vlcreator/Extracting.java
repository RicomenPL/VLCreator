package leszcz_team.vlcreator;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


public class Extracting {

    Document page;
    Elements link;
    String ink;
    String video_url;
    String checkMobileString;

    Extracting(Document doc){
        page = doc;
    }

    public String start(){

        link = page.select(".brdPlayerWrapper > .brdPlayer > div");
        ink = link.attr("player_data");
        JsonObject jsonObject1 = new JsonParser().parse(ink).getAsJsonObject();
        String step1 = jsonObject1.get("video").toString();
        JsonObject jsonObject2 = new JsonParser().parse(step1).getAsJsonObject();
        video_url = jsonObject2.get("file").getAsString();
        video_url = checkMobile(video_url);

        return video_url;
    }

    public String checkMobile (String key){

        checkMobileString = key.substring(key.length() - 3);

        if (checkMobileString.equals("mp4")){
            String code1 = key;
            return code1;
        }
        else{
            String code = key + ".mp4";
            return code;
        }
    }
}
